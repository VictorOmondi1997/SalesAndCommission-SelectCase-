﻿Public Class Form1
    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        Dim saleAmount As Long
        saleAmount = txtSale.Text
        Select Case saleAmount
            Case Is <= 100000
                txtComm.Text = Val(saleAmount * 0.1)
            Case Is <= 200000
                txtComm.Text = Val(saleAmount * 0.15)
            Case Is <= 300000
                txtComm.Text = Val(saleAmount * 0.2)
            Case Is <= 400000
                txtComm.Text = Val(saleAmount * 0.25)
            Case Else
                txtComm.Text = Val(saleAmount * 0.275)
        End Select
    End Sub
End Class
